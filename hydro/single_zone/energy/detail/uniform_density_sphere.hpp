////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro_helper.cpp
//! \brief A file to define useful hydro helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define S_DAMP_RATE              "damping_rate"
#define S_E_LOSS_FACTOR          "e_loss_factor"
#define S_HYDRO_EQUIL            "hydro_equil"
#define S_LUM_FACTOR             "lum_factor"
#define S_MASS                   "mass"
#define S_R_0                    "R_0"
#define S_SCALED_VELOCITY_0      "v_0"
#define S_VELOCITY               "v"

#define HYDRO_EVOLVE

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

class hydro_detail : public thermo
{

  public:

    hydro_detail(){}
    hydro_detail( v_map_t& v_map ) : thermo()
    {
      d_t9_0 = v_map[nnt::s_T9_0].as<double>();
      d_e_loss_factor = v_map[S_E_LOSS_FACTOR].as<double>();
      d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      d_lum_factor = v_map[S_LUM_FACTOR].as<double>();
      d_damp_rate = v_map[S_DAMP_RATE].as<double>();
      b_hydro_equil = v_map[S_HYDRO_EQUIL].as<double>();

      if( v_map.count( nnt::s_RHO_0 ) )
      {
        d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      }

      if( v_map.count( S_R_0 ) )
      {
        d_R_0 = v_map[S_R_0].as<double>();
      }
 
      if( v_map.count( S_MASS ) )
      {
        d_mass = v_map[S_MASS].as<double>() * GSL_CONST_CGSM_SOLAR_MASS;
      }

      if( !b_hydro_equil )
      {

        if( !v_map.count( nnt::s_RHO_0 ) )
        {
          d_RHO_0 =
            3. * d_mass / ( 4. * M_PI * gsl_pow_3( d_R_0 ) );
        }
 
        if( !v_map.count( S_R_0 ) )
        {
          d_R_0 =
            pow(
              3. * d_mass / ( 4. * M_PI * d_rho_0 ),
              1. / 3.
            );
        }
 
        if( !v_map.count( S_MASS ) )
        {
          d_mass =
            d_rho_0 * ( 4. * M_PI * gsl_pow_3( d_R_0 ) / 3. );
        }

      }
      else
      {
        if(
          !v_map.count( nnt::s_RHO_0 ) ||
          v_map.count( S_R_0 ) ||
          v_map.count( S_MASS )
        )
        {
          std::cerr <<
            "Hydrostatic equilibrium only takes initial rho." << std::endl;
          exit( EXIT_FAILURE );
        }
      }
      else
      {
        size_t i_count =
          v_map.count( nnt::s_RHO_0 ) +
          v_map.count( S_R_0 ) +
          v_map.count( S_MASS );
        if( i_count != 2 )
        {
          std::cerr << "Supply two and only two of " <<
                       nnt::s_RHO_0 << "  " <<
                       S_R_0 << "  " <<
                       S_MASS << std::endl;
          exit( EXIT_FAILURE );
        }
      }
 
    }

    state_type
    set( nnt::Zone& zone )
    {
      state_type x, dxdt;
      x.push_back( 1 );
      x.push_back( d_y_0 );
      x.push_back( d_t9_0 );
      return x;
    }

    void
    initialize( state_type& x, nnt::Zone& zone )
    {

      registerThermoFunctions( zone );

      d_P_0 = computePressure( zone );
      if( b_hydro_equil )
      {
        d_R_0 =
          pow(
            ( 3.* d_P_0 ) /
            ( 4.* M_PI * GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT *
              gsl_pow_2( d_rho_0 )
            ),
            1. / 2.
          );
        d_mass = 4. * d_rho_0 * M_PI * gsl_pow_3( d_R_0 ) / 3.;
      }
          
      d_tau_0_2 = d_rho_0 * gsl_pow_2( d_R_0 ) / d_P_0;
      d_tau_1_2 =
        gsl_pow_3( d_R_0 ) / ( GSL_CONST_CGSM_GRAVITATIONAL_CONSTANT * d_mass );

    }

    double computeRho( const state_type& x, double d_t, nnt::Zone& zone )
    {
      return d_rho_0 / gsl_pow_3( x[0] );
    }

    double computeDlnRhoDt(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {
      return -3. * x[1] / x[0];
    }

    double
    computeHeatingRate(
      const state_type& x,
      double d_t,
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {
      return
        d_damp_rate *
          gsl_pow_3( x[0] ) *
          gsl_pow_2( x[1] * d_R_0 ) / GSL_CONST_NUM_AVOGADRO
        -
        d_lum_factor * 3. * gsl_pow_2( x[0] ) *
            GSL_CONST_CGSM_STEFAN_BOLTZMANN_CONSTANT *
            gsl_pow_4( x[2] * GSL_CONST_NUM_GIGA ) /
            ( d_rho_0 * GSL_CONST_NUM_AVOGADRO * d_R_0 )
        - 
        computeEnergyLossRate( zone, p_view );
    }

    double computeEnergyLossRate(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {

      Libnucnet__NetView * p_view2 =
        zone.getNetView(
          "",
          "[product[contains(.,'neutrino')]]"
        );

      Libnucnet__NetView * p_new = getLossView( p_view, p_view2 );

      double d_result =
        d_e_loss_factor *
        computeEnergyGenerationRate( zone, p_new );

      Libnucnet__NetView__free( p_new );

      return d_result;

    }
      
    double
    computeAcceleration(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {

      return
        ( gsl_pow_2( x[0] ) / d_tau_0_2 )
        *
        (
          ( computePressure( zone ) / d_P_0 ) -
          ( d_tau_0_2 / d_tau_1_2 ) * ( 1. / gsl_pow_4( x[0] ) )
        )
        -
        x[1] * gsl_pow_3( x[0] ) * d_damp_rate;
    }

    void
    updateOtherZoneProperties(const state_type& x, nnt::Zone& zone )
    {

      zone.updateProperty( nnt::s_RADIUS, x[0] * d_R_0 );
      zone.updateProperty( S_VELOCITY, x[1] * d_R_0 );

      zone.updateProperty(
        nnt::s_PRESSURE,
        user::compute_thermo_quantity( zone, nnt::s_PRESSURE, nnt::s_TOTAL )
      );

      zone.updateProperty(
        nnt::s_ENTROPY_PER_NUCLEON,
        user::compute_thermo_quantity(
          zone, nnt::s_ENTROPY_PER_NUCLEON, nnt::s_TOTAL
        )
      );

    }

  private:
    double d_mass, d_t9_0, d_P_0, d_R_0;
    double d_y_0, d_rho_0, d_damp_rate;
    double d_tau_0_2, d_tau_1_2;
    double d_e_loss_factor, d_lum_factor;
    bool b_hydro_equil;

};

//##############################################################################
// hydro_detail_options().
//##############################################################################

class hydro_detail_options
{

  public:
    hydro_detail_options() {}

    void
    getDetailOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial T (in 10^9 K)"
          )

          ( nnt::s_RHO_0, po::value<double>(),
            "Initial density (in g/cc)"
          )

          ( S_MASS, po::value<double>(),
            "Sphere mass (solar masses)"
          )

          ( S_R_0, po::value<double>(),
            "Sphere radius (cm)"
          )

          ( S_SCALED_VELOCITY_0, po::value<double>()->default_value( 0., "0." ),
            "Scaled initial velocity"
          )

          ( S_HYDRO_EQUIL,
            po::value<bool>()->default_value( false, "false" ),
            "Start the sphere in hydrostatic equilibrium"
          )

          ( S_DAMP_RATE, po::value<double>()->default_value( 0., "0." ),
            "Damping (per s)"
          )

          ( S_E_LOSS_FACTOR, po::value<double>()->default_value( 0.6, "0.6" ),
            "Neutrino loss factor"
          )

          ( S_LUM_FACTOR, po::value<double>()->default_value( 0., "0." ),
            "Luminosity factor"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

    void
    checkInput( po::variables_map& v_map )
    {
    }

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 1 " +
        "--" + std::string( nnt::s_RHO_0 ) + " 1.e12 " +
        "--" + std::string( S_MASS ) + " 0.01 ";

    }

};

}  // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
