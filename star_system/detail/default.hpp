//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute properties of stars.
////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "star_system/base/star_system_base.hpp"

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef NNP_STAR_DETAIL_HPP
#define NNP_STAR_DETAIL_HPP

#define D_YEAR    3.15e7

#include <boost/tuple/tuple.hpp>

namespace wn_user
{

namespace detail
{

#define  S_GRID_X              "grid x"
#define  S_GRID_Y              "grid y"
#define  S_GRID_Z              "grid z"
#define  S_GRID_X_FACTOR       "grid x factor"
#define  S_GRID_Y_FACTOR       "grid y factor"
#define  S_GRID_Z_FACTOR       "grid z factor"
#define  S_ORIGINAL_ZONE_ID    "original zone id"
#define  S_STAR_ID             "star id"
#define  S_NUMBER              "number"
#define  S_STANDARD_DEVIATION  "standard deviation"

class Star : public star_system_base
{

  public:
    Star() : star_system_base() {}

    double
    computeLifetime( double d_star_mass )
    {
      return 1.e10 * D_YEAR / pow( d_star_mass, 2 );
    }

    void
    setGridLocation( boost::tuple<int,int,int> t )
    {
      updateProperty( S_GRID_X, t.get<0>() );
      updateProperty( S_GRID_Y, t.get<0>() );
      updateProperty( S_GRID_Z, t.get<0>() );
    }

    void
    setSubGridLocation( boost::tuple<double,double,double> u )
    {
      updateProperty( S_GRID_X_FACTOR, u.get<0>() );
      updateProperty( S_GRID_Y_FACTOR, u.get<1>() );
      updateProperty( S_GRID_Z_FACTOR, u.get<2>() );
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_STAR_DETAIL_HPP
