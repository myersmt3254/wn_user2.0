////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// @cond _NETWORK_DATA_FICTITIOUS_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file fictitious.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_data/base/network_data.hpp"

#ifndef NNP_NETWORK_DATA_DETAIL_HPP
#define NNP_NETWORK_DATA_DETAIL_HPP

#define S_FICTITIOUS_FILE  "fictitious_file"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::network_data
{

  public:
    network_data( v_map_t& v_map ) : base::network_data( v_map )
    {

    }

    void operator()()
    {

      base::network_data::setData();

      if( v_map.count( S_FICTITIOUS_FILE ) )
      {
        unsigned int i_z, i_a; 
        std::string s_xsec;
        double d_f_x;
        std::vector<std::string> xsec;
        Libnucnet__Net * p_net = Libnucnet__getNet( getNucnet() );
        std::ifstream my_file;
        my_file.open( s_fictitious_file.c_str() )
        while( my_file >> i_z >> i_a >> s_xsec >> d_f_x )
        {
           Libnucnet__Species * p_species =
             Libnucnet__Species__new(
               i_z,
               i_a,
               "fictitious",
               0,
               "",
               0.,
               0.,
               NULL,
               NULL
             );
           Libnucnet__Nuc__addSpecies(
             Libnucnet__Net__getNuc( p_net ),
             p_species
           );
           f_sp.push_back( p_species );
           xsec.push_back( s_xsec );
           f_x.push_back( d_f_x );
        }
        for( size_t i = 0; i < f_sp.size() - 1; i++ )
        {
          if( boost::lexical_cast<double>( xsec[i] ) > 0 )
          {
            Libnucnet__Reaction * p_reaction = Libnucnet__Reaction__new();
            Libnucnet__Reaction__addReactant(
              p_reaction,
              Libnucnet__Species__getName( f_sp[i] )
            );
            Libnucnet__Reaction__addReactant( p_reaction, "n"  );
            Libnucnet__Reaction__addProduct(
              p_reaction,
              Libnucnet__Species__getName( f_sp[i+1] )
            );
            Libnucnet__Reaction__addProduct( p_reaction, "gamma"  );
            Libnucnet__Reaction__setUserRateFunctionKey(
              p_reaction,
              "constant sigma"
            );
            Libnucnet__Reaction__updateUserRateFunctionProperty(
              p_reaction, "sigma", NULL, NULL, xsec[i].c_str()
            );
            Libnucnet__Reac__addReaction(
              Libnucnet__Net__getReac( p_net ), p_reaction
            );
          }
        }
      }
    }

};
    
//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : base::network_data_options
{

  public:
    network_data_options() : base::network_data_options() {}

    std::string
    getExample()
    {
      return base::network_data_options::getExample();
    }

    void
    get( po::options_description& network_data )
    {

      try
      {

        network_data.add_options()

        // Option for fictitious file
        (
          S_FICTITIOUS_FILE, po::value<std::string>(),
          "File with fictitious species data."
        )

        ;

        base::network_data_options::get( network_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_NETWORK_DATA_DETAIL_HPP

/// @endcond
