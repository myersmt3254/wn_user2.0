////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// \file

#include "my_global_types.h"

#ifndef NNP_NETWORK_DATA_HPP
#define NNP_NETWORK_DATA_HPP

namespace wn_user
{

namespace network_data
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class network_data
///
/// \brief A class to handle network data.
///
/// Network data include data about nuclides and the reactions among them.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data().
//##############################################################################

class network_data : public detail::network_data
{

/// The constructor.
/// \param v_map The options variable map.

  public: network_data( v_map_t& v_map ) : detail::network_data( v_map ) { }

};
    
//##############################################################################
// options().
//##############################################################################

////////////////////////////////////////////////////////////////////////////////
///
/// \class options
///
/// \brief A class to handle the network data options.  The user does not
///        normally access this class, which is usually called from the
///        inputter class.
///
////////////////////////////////////////////////////////////////////////////////

class options : detail::options
{

  public:
    options() : detail::options() {}

/// Routine to return the example string.
/// \return An example string.

    std::string
    getExample()
    {

      return detail::options::getExample();

    }

/// Routine to get the options.
///
/// \param o_map The options map.
///
/// \return On successful return, the options map has been updated.

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description options_desc( "\nNetwork data Options" );

        detail::options::get( options_desc );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "network_data",
            options_struct( options_desc, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace network_data

} // namespace wn_user

#endif // NNP_NETWORK_DATA_HPP
