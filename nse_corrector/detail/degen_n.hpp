////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file screening.hpp
//! \brief A file to define nse_correction routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/shared_ptr.hpp>
#include "my_global_types.h"

#include "nse_corrector/base/nse_corrector_base.hpp"

#include "nnt/iter.h"

#ifndef NNP_NSE_CORRECTOR_DETAIL_HPP
#define NNP_NSE_CORRECTOR_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// nse_exp_factor().
//##############################################################################

double
nse_exp_factor(
  Libnucnet__Nuc *p_nuc,
  Libnucnet__Species *p_species,
  double d_T9,
  double d_rho
)
{

  return
    log(
      Libnucnet__Species__computeQuantumAbundance(
        p_species,
        d_T9,
        d_rho
      )
    ) -
    Libnucnet__Nuc__computeSpeciesBindingEnergy(
      p_nuc,
      p_species
    ) /
    (
      d_T9 *
      GSL_CONST_NUM_GIGA *
      ( GSL_CONST_NUM_MICRO / GSL_CONST_CGSM_ELECTRON_VOLT ) *
      GSL_CONST_CGSM_BOLTZMANN
    );

}

//##############################################################################
// nse_correction_data().
//##############################################################################

class
nse_correction_data
{

  public:
    nse_correction_data( nnt::Zone& _zone ) : zone( _zone )
    { dT9 = 0; dRho = 0; dYn = 0; }

    double
    getData()
    {

      double d_T9 = zone.getProperty<double>( nnt::s_T9 );
      double d_rho = zone.getProperty<double>( nnt::s_RHO );

      Libnucnet__Species * p_species =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          "n"
        );

      double d_yn =
        Libnucnet__Zone__getSpeciesAbundance( zone.getNucnetZone(), p_species );

      if( d_T9 != dT9 || d_rho != dRho || d_yn != dYn )
      {

        Libstatmech__Fermion * p_fermion =
          Libstatmech__Fermion__new(
            "n",
            GSL_CONST_CGSM_UNIFIED_ATOMIC_MASS *
              gsl_pow_2( GSL_CONST_CGSM_SPEED_OF_LIGHT ) *
              GSL_CONST_NUM_MICRO / GSL_CONST_CGSM_ELECTRON_VOLT *
              Libnucnet__Species__getA( p_species ) +
              Libnucnet__Species__getMassExcess( p_species ),
            2,
            0.
          );

        dResult =
          Libstatmech__Fermion__computeChemicalPotential(
            p_fermion,
            d_T9 * GSL_CONST_NUM_GIGA,
            d_yn * d_rho * GSL_CONST_NUM_AVOGADRO,
            NULL,
            NULL
          ) -
          log(
            d_yn /
            Libnucnet__Species__computeQuantumAbundance(
              p_species,
              d_T9,
              d_rho
            )
          );

        Libstatmech__Fermion__free( p_fermion ) ;

        dT9 = d_T9;
        dRho = d_rho;
        dYn = d_yn;

      }

      return dResult;

    }

  private:
    nnt::Zone& zone;
    double dResult;
    double dT9;
    double dRho;
    double dYn;

};

//##############################################################################
// get_nse_correction_data().
//##############################################################################

boost::shared_ptr<nse_correction_data>
get_nse_correction_data( nnt::Zone& zone )
{
  boost::shared_ptr<nse_correction_data> p( new nse_correction_data( zone ) );
  return p;
}

//##############################################################################
// nse_correction().
//##############################################################################

double
nse_correction(
  Libnucnet__Species *p_species,
  double d_T9,
  double d_rho,
  double d_ye,
  void * p_data
)
{

  if( d_ye < 0. || d_ye > 1. )
  {
    fprintf( stderr, "Invalid Ye" );
    exit( EXIT_FAILURE );
  }

  boost::shared_ptr<nse_correction_data> p =
    boost::any_cast<boost::shared_ptr<nse_correction_data> >(
      *(boost::any *) p_data
    );

  if( strcmp( Libnucnet__Species__getName( p_species ), "n" ) == 0 )
  {
    return p->getData( );
  }

  return 0;

}

//##############################################################################
// nse_corrector().
//##############################################################################

class nse_corrector : public nse_corrector_base
{

  public:
    nse_corrector() : nse_corrector_base() {}
    nse_corrector( v_map_t& v_map ) : nse_corrector_base( v_map ){}

    void setNseCorrector( nnt::Zone& zone )
    {
      if( useNseCorrector() )
      {
        Libnucnet__Zone__setNseCorrectionFactorFunction(
          zone.getNucnetZone(),
          (Libnucnet__Species__nseCorrectionFactorFunction) nse_correction,
          NULL
        );

        zone.updateFunction(
          nnt::s_NSE_CORRECTION_FACTOR_DATA_FUNCTION,
          static_cast<boost::function<boost::any()> >(
            boost::bind( get_nse_correction_data, boost::ref( zone ) )
          )
        );
      }
    } 

};

//##############################################################################
// nse_corrector_options().
//##############################################################################

class nse_corrector_options : public nse_corrector_base_options
{

  public:
    nse_corrector_options() : nse_corrector_base_options() {}

    void
    getDetailOptions( po::options_description& nse_corrector )
    {

      try
      {

        getBaseOptions( nse_corrector );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

}  // namespace detail

}  // namespace wn_user

#endif  // NNP_NSE_CORRECTOR_DETAIL_HPP
