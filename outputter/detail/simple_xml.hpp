////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "outputter/base/xml.hpp"
#include "outputter/base/outputter.hpp"

#ifndef NNP_OUTPUTTER_SIMPLE_XML_HPP
#define NNP_OUTPUTTER_SIMPLE_XML_HPP

#define S_INDENT     "indent"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// outputter_simple_xml().
//##############################################################################

class outputter_simple_xml : public base::outputter_xml, public base::outputter
{

  public:
    outputter_simple_xml( v_map_t& v_map ) :
      base::outputter_xml( v_map ), base::outputter( v_map )
    {
      iIndent = v_map[S_INDENT].as<int>();
    }

    void set( Libnucnet * p_nucnet )
    {
      base::outputter_xml::set( p_nucnet );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        (*this)( zone );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      if( !getXmlFile().empty() )
      {
        Libnucnet__Zone * p_copy =
          Libnucnet__Zone__copy( zone.getNucnetZone() );
        Libnucnet__addZone( getOutputNucnet(), p_copy );
      }
    }

    void write()
    {
      if( !getXmlFile().empty() )
      {
	prepOutput1();
        Libnucnet__updateZoneXmlMassFractionFormat(
          getOutputNucnet(),
          getXmlFormat().c_str()
        );

        Libnucnet__writeToXmlFileWithTextWriter(
          getOutputNucnet(),
          getXmlFile().c_str(),
          iIndent
        );
	prepOutput2();

      }

    }

  private:
    int iIndent;

};

//##############################################################################
// outputter_simple_xml_options()
//##############################################################################

class outputter_simple_xml_options : public base::outputter_xml_options,
  public base::outputter_options
{

  public:
    outputter_simple_xml_options() :
      base::outputter_xml_options(), base::outputter_options() {}

    std::string
    getExample()
    {
      return base::outputter_options::getExample();
    }

    void operator()( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()
      
        ( S_INDENT, po::value<int>()->default_value( 1 ),
          "Indent xml output (1) or not (0)" )
    
        ;

        base::outputter_xml_options::getOptions( outputter );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_OUTPUTTER_SIMPLE_XML_HPP
