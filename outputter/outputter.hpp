////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////


#ifndef NNP_OUTPUTTER_HPP
#define NNP_OUTPUTTER_HPP

#include "outputter/base/outputter.hpp"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// outputter().
//##############################################################################

class outputter
{
  public:
    outputter( v_map_t& v_map )
    {

#ifdef NNP_OUTPUTTER_XML_HPP
      f_vec.push_back( new detail::outputter_xml( v_map ) );
#endif

#ifdef NNP_OUTPUTTER_SIMPLE_XML_HPP
      f_vec.push_back( new detail::outputter_simple_xml( v_map ) );
#endif

#ifdef NNP_OUTPUTTER_XML_TEXT_WRITER_HPP
      f_vec.push_back( new detail::outputter_xml_text_writer( v_map ) );
#endif

#ifdef NNP_OUTPUTTER_HDF5_HPP
      f_vec.push_back( new detail::outputter_hdf5( v_map ) );
#endif

    }

    void set( Libnucnet * p_nucnet )
    {
      BOOST_FOREACH( base::outputter& f, f_vec )
      {
        f.set( p_nucnet );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      BOOST_FOREACH( base::outputter& f, f_vec )
      {
        f( zone );
      }
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( base::outputter& f, f_vec )
      {
        f( zones );
      }
    }

    void write()
    {
      BOOST_FOREACH( base::outputter& f, f_vec )
      {
        f.write();
      }
    }

  private:
    boost::ptr_vector<base::outputter> f_vec;

};

//##############################################################################
// outputter_options()
//##############################################################################

class outputter_options
{

  public:
    outputter_options(){}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description outputter("\nOutputter options");

        ;

#ifdef NNP_OUTPUTTER_SIMPLE_XML_HPP
        o_vec.push_back( new detail::outputter_simple_xml_options() );
#endif

#ifdef NNP_OUTPUTTER_XML_HPP
        o_vec.push_back( new detail::outputter_xml_options() );
#endif

#ifdef NNP_OUTPUTTER_HDF5_HPP
        o_vec.push_back( new detail::outputter_hdf5_options() );
#endif

#ifdef NNP_OUTPUTTER_XML_TEXT_WRITER_HPP
        o_vec.push_back( new detail::outputter_xml_text_writer_options() );
#endif

        std::string s_example = "";
        BOOST_FOREACH( base::outputter_options& f, o_vec )
        {
          f( outputter );
          s_example += f.getExample();
        } 

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "outputter", 
            options_struct( outputter, s_example )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

  private:
    boost::ptr_vector<base::outputter_options> o_vec;

};

} // namespace wn_user

#endif // NNP_OUTPUTTER_HPP
